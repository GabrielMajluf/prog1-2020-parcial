﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class EnvioFragil : Envio
    {
        //SE PUEDE USAR UN ENUMERADOR, NO BOOLEANO PORQ SON 3 VALORES
        public bool Liquido { get; set; } //Si no es liquido (Inflamable o Tóxico), considero Explosivo. 

        public override double CalcularCostoEnvio()
        {
            if (Liquido)
            {
                if (CodigoPostalOrigen == CodigoPostalDestino)
                {
                    return 240;
                }
                else
                {
                    return 240 * 1.07;
                }
            }
            else
            {
                if (CodigoPostalOrigen == CodigoPostalDestino)
                {
                    return 275;
                }
                else
                {
                    return 240 * 1.07;
                }
            }
        }
        public override DateTime CalcularFechaEntregaEstimada()
        {
            return DateTime.Today.AddDays(5);
        }
    }
}
