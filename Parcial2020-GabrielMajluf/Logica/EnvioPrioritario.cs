﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class EnvioPrioritario : Envio
    {
        public override double CalcularCostoEnvio()
        {
            double PesoExcedente;
            if (CodigoPostalOrigen == CodigoPostalDestino)
            {
                if (PesoPaquete > 5)
                {
                    PesoExcedente = PesoPaquete - 5;
                    return 120 + PesoExcedente * 12.5;
                }
                return 120; 
            }
            else
            {
                if (PesoPaquete > 5)
                {
                    if (PesoPaquete > 5)
                    {
                        PesoExcedente = PesoPaquete - 5;
                        return 180 + PesoExcedente * 12.5;
                    }
                }
                return 180;
            }
        }
        public override DateTime CalcularFechaEntregaEstimada()
        {
            double PesoExcedente;
            if (CodigoPostalOrigen == CodigoPostalDestino)
            {
                if (PesoPaquete > 5)
                {
                    PesoExcedente = PesoPaquete - 5;
                    Convert.ToInt32(PesoExcedente);
                    return DateTime.Today.AddDays(PesoExcedente);
                }
                return DateTime.Today;
            }
            else
            {
                if (PesoPaquete > 5)
                {
                    PesoExcedente = PesoPaquete - 5;
                    Convert.ToInt32(PesoExcedente);
                    return DateTime.Today.AddDays(PesoExcedente+1);
                }
                return DateTime.Today.AddDays(1);
            }
        }
    }
}
