﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class EnvioNormal : Envio
    {
        public override double CalcularCostoEnvio()
        {
            return (10.25 * VolumenPaquete) + (5.75 * PesoPaquete);
        }
        public override DateTime CalcularFechaEntregaEstimada()
        {
            return DateTime.Today.AddDays(3);
        }
    }
}
