﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public abstract class Envio
    {
        public int CodigoPostalOrigen { get; set; }
        public int CodigoPostalDestino { get; set; }
        public int CodigoSeguimiento { get; set; }
        public float VolumenPaquete { get; set; }
        public float PesoPaquete { get; set; }
        public DateTime FechaIngreso { get; set; }
        public DateTime FechaEntrega { get; set; }
        public double CostoEnvio { get; set; }
        public Cliente Cliente { get; set; }

        //ESTE METODO DEBERIA SER UNO SOLO
        public abstract double CalcularCostoEnvio();
        public abstract DateTime CalcularFechaEntregaEstimada();


        public int CalcularCodigoSeguimiento()
        {
            Random NumeroAleatorio = new Random();
            return NumeroAleatorio.Next(000000, 999999);
        }
        
        public bool CalcularEntregado()
        {
            if (FechaEntrega > DateTime.Today)
            {
                return true;
            }
            return false;
        }
    }
}
