﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Principal
    {
        List<Envio> ListaEnvios = new List<Envio>();

        //PODRIA LLAMARSE "CARGAR ENVIO"
        public void CargarEnvioNormal(int pcodPostalOrigen, int pcodPostalDestino, float pVolPaquete, float pPesoPaquete, Cliente pCliente, int pTipoEnvio)
        {
            //Como no se puede instanciar la clase Envio al ser abstracta para luego utilizar IS en cada Subclase, Utilizo la variable pTipoEnvio, ++ 
            //referenciando a que lo va a pasar el cliente en un futuro.

            //SE DUPLICA MUCHO CODIGO
            if (pTipoEnvio == 1)
            {
                EnvioNormal EnvioNormal = new EnvioNormal();
                EnvioNormal.CodigoPostalOrigen = pcodPostalOrigen;
                EnvioNormal.CodigoPostalDestino = pcodPostalDestino;
                EnvioNormal.VolumenPaquete = pVolPaquete;
                EnvioNormal.PesoPaquete = pPesoPaquete;
                EnvioNormal.Cliente = pCliente;
                EnvioNormal.FechaIngreso = DateTime.Today;
                EnvioNormal.FechaEntrega = EnvioNormal.CalcularFechaEntregaEstimada();
                EnvioNormal.CostoEnvio = EnvioNormal.CalcularCostoEnvio();
                EnvioNormal.CodigoSeguimiento = EnvioNormal.CalcularCodigoSeguimiento();
                ListaEnvios.Add(EnvioNormal);
            }
            if (pTipoEnvio == 2)
            {
                EnvioPrioritario EnvioPrioritario = new EnvioPrioritario();
                EnvioPrioritario.CodigoPostalOrigen = pcodPostalOrigen;
                EnvioPrioritario.CodigoPostalDestino = pcodPostalDestino;
                EnvioPrioritario.VolumenPaquete = pVolPaquete;
                EnvioPrioritario.PesoPaquete = pPesoPaquete;
                EnvioPrioritario.Cliente = pCliente;
                EnvioPrioritario.FechaIngreso = DateTime.Today;
                EnvioPrioritario.FechaEntrega = EnvioPrioritario.CalcularFechaEntregaEstimada();
                EnvioPrioritario.CostoEnvio = EnvioPrioritario.CalcularCostoEnvio();
                EnvioPrioritario.CodigoSeguimiento = EnvioPrioritario.CalcularCodigoSeguimiento();
                ListaEnvios.Add(EnvioPrioritario);
            }
            if (pTipoEnvio == 3)
            {
                EnvioFragil EnvioFragil = new EnvioFragil();
                EnvioFragil.CodigoPostalOrigen = pcodPostalOrigen;
                EnvioFragil.CodigoPostalDestino = pcodPostalDestino;
                EnvioFragil.VolumenPaquete = pVolPaquete;
                EnvioFragil.PesoPaquete = pPesoPaquete;
                EnvioFragil.Cliente = pCliente;
                EnvioFragil.FechaIngreso = DateTime.Today;
                EnvioFragil.FechaEntrega = EnvioFragil.CalcularFechaEntregaEstimada();
                EnvioFragil.CostoEnvio = EnvioFragil.CalcularCostoEnvio();
                EnvioFragil.CodigoSeguimiento = EnvioFragil.CalcularCodigoSeguimiento();
                ListaEnvios.Add(EnvioFragil);
            }
        }


        public List<EnvioxCliente> ObtenerListaEnvioxCliente(Cliente pcliente)
        {
            List<EnvioxCliente> Resultado = new List<EnvioxCliente>();

            foreach (var envio in ListaEnvios)
            {
                if (envio.Cliente == pcliente)
                {
                    EnvioxCliente envioxcliente = new EnvioxCliente();
                    envioxcliente.Nombre = envio.Cliente.Nombre;
                    envioxcliente.FechaIngreso = envio.FechaIngreso;
                    envioxcliente.Entregado = envio.CalcularEntregado();
                    Resultado.Add(envioxcliente);

                }
            }
            Resultado = Resultado.OrderByDescending(x => x.FechaIngreso).ToList();
            return Resultado;
        }

        //NO LO VEO NECESARIO
        public int ValidarCodigoSeguimiento(Envio pEnvio)
        {
            int CodSeguimiento = pEnvio.CalcularCodigoSeguimiento();
            int NroValidador = 0;
            while (NroValidador == 0)
            {
                foreach (var envio in ListaEnvios)
                {
                    if (envio.CodigoSeguimiento == CodSeguimiento)
                    {
                        NroValidador += 1;
                    }
                }
                if (NroValidador == 0)
                {
                    return CodSeguimiento;
                }
                else
                {
                    NroValidador = 0;
                    CodSeguimiento = pEnvio.CalcularCodigoSeguimiento();
                }
            }
            return 0;
        }
    }
}
